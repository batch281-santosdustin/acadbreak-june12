const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/userRoutes');
const roomRoutes = require('./routes/roomRoutes');

const app = express();

// [SECTION] Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// [SECTION] Defines the string to be included for all user routes defined in the Routes file
app.use('/users',userRoutes);
app.use('/rooms',roomRoutes);

// [SECTION] DB Connection
mongoose.set('strictQuery', true);
mongoose.connect("mongodb+srv://santosdustinaaron07:Ayu2LjfBRn3QDOpQ@wdc028-course-booking.fhfpbwj.mongodb.net/AcadBreak-June12?retryWrites=true&w=majority",
{
	// Deprecators: It will give warnings if there are concerns
	useNewUrlParser : true,
	useUnifiedTopology : true
});

let db = mongoose.connection;
db.on('error', console.error.bind(console, "MongoDB Connection Error."));
db.once('open', () => console.log('Now connected to MongoDB Atlas!'));

// [SECTION] Listen
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT|| 4000}` )
})