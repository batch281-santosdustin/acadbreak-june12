const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	firstName : {
		type: String,
		required : [true, "First name is required."]
	},
	lastName : {
		type: String,
		required : [true, "Last name is required."]
	},
	email : {
		type: String,
		required : [true, "Email is required."]
	},
	password : {
		type : String,
		required : [true, "Password is required."]
	},
	isAdmin : {
		type: Boolean,
		default: false
	},
	mobileNo : {
		type : String,
		required : [true, "Mobile number is required."]
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	address:{ 
		// type: String, 
		// required: [true, "address is required"]
		houseNo: {
		    type: String,
		    required: [true, "House number is required."]
		},
		street: {
		    type: String,
		    required: [true, "Street name is required."]
		},
		barangay: {
		    type: String,
		    required: [true, "Barangay is required."]
		},
		city: {
		    type: String,
		    required: [true, "City is required."]
		},
		province: {
		    type: String,
		    required: [true, "Province is required."]
		},
		country: {
			type: String,
			required: [true, "Country is required."]
		}
	}
})

module.exports = mongoose.model("User", userSchema);