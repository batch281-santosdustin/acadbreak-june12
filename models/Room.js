const mongoose = require('mongoose');

const roomSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Room name is required"]
	},
	description : {
		type: String,
		required: [true, "Room description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required."]
	},
	capacity: {
		type: Number,
		required: [true, "Room capacity is required."]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	isOnSale: {
		type: Boolean,
		default: false
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	guestList: [{
		userId: {
			type: String
		},
		bookedOn: {
			type: Date,
			default: new Date()
		}
	}]
})

module.exports = mongoose.model('Room', roomSchema);