const express = require("express");
const router = express.Router();
const roomController = require("../controllers/roomController");
const auth = require("../auth");

// [SECTION] No params
// Add Room IFF ADMIN
router.post("/", auth.verify, (req, res) => {
	const data = {
		room: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true) {
		roomController.addRoom(data).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
})

// Get all available room IFF ADMIN
router.get("/all", auth.verify, (req, res) => {
	const data = {
		room: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true) {
		roomController.getAllRooms().then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
})

// Retrieve all available rooms
router.get("/active", (req, res) => {
	roomController.getActiveRooms(req.params).then(resultFromController => res.send(resultFromController));
})

// [SECTION] With params
// Retrieve a single room by using its ID
router.get("/:roomId", (req, res) => {
	console.log(req.params.roomId);

	roomController.getRoom(req.params).then(resultFromController => res.send(resultFromController));
})

module.exports = router;