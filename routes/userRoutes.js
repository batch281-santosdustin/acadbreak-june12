// Dependencies
const express = require('express');
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require('../auth');

// [SECTION] No params
// Route for user registration
router.post("/register", (req, res) => {
	userController.register(req.body).then(resultFromController => res.send(resultFromController));
})

/*router.post('/register', userController.registration);*/

// Route for authentication
router.post("/login", (req, res) => {
	userController.login(req.body)
	.then(resultFromController => res.send(resultFromController));	
})


module.exports = router;