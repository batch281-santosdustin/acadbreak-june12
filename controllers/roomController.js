const Room = require("../models/Room");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Adding room
module.exports.addRoom = (data) => {
	let newRoom = new Room ({
		name: data.room.name,
		description: data.room.description,
		price: data.room.price,
		capacity: data.room.capacity
	})

	return newRoom.save().then((course, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	})
}

// Retrieving all rooms
module.exports.getAllRooms = () => {
	return Room.find({}).then(result => {
		return result;
	})
}

// Retrieving all active rooms
module.exports.getActiveRooms = () => {
	return Room.find({isActive: true}).then(result => {
		return result;
	})
}

// Retrieving a specific room
module.exports.getRoom = (reqParams) => {
	return Room.findById(reqParams.roomId).then(result => {
		return result;
	})
}