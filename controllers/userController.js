// Dependencies
const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Course = require("../models/Room");

// User registration and availability
module.exports.register = (reqBody, res) => {
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo,
		// address: reqBody.address
		address: ({
			houseNo: reqBody.address.houseNo,
			street: reqBody.address.street,
			barangay: reqBody.address.barangay,
			city: reqBody.address.city,
			province: reqBody.address.province,
			country: reqBody.address.country
		})

	})

	return User.find({email: reqBody.email})
		.then(result => {
			if(result.length > 0) {
				return false;
			} else {
				return newUser.save()
					.then(user => {
						console.log(newUser);
						return true;
					})
					.catch(err => {
						return false;
					})
			}
		})
}

// User authentication/ Login
module.exports.login = (reqBody) => {
	return User.findOne({email: reqBody.email})
		.then(result => {
			if(!result){
				return false;
			} else{
				const checkPassword = bcrypt.compareSync(reqBody.password, result.password);

				if(checkPassword){
					return {accessToken: auth.createToken(result)};
				} else {
					return false;
				}
			}
		})
}